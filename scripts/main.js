"use strict"


const createNewUser = () => {
    return {
        firstName: prompt('Enter your name:', 'firstName'),
        lastName: prompt('Enter your last name:','lastName'),
        getLogin: function() {
            let login = this.firstName[0] + this.lastName;
            return login.toLowerCase();
        },
        getPassword: function () {
          return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6);
        },
        birthday: prompt('Enter date your birthday: dd.mm.yyyy', 'dd.mm.yyyy'),
        getAge: function () {
            let date = new Date ();
            let birthdayUser = this.birthday.slice(6, 10) + '-' + this.birthday.slice(3, 5) + '-' + this.birthday.slice(0, 2);
            let dayBirthday = new Date(birthdayUser)
            if (date.getMonth() > dayBirthday.getMonth() ) {
                return date.getFullYear() - dayBirthday.getFullYear();
            } return date.getFullYear() - dayBirthday.getFullYear() - 1;
        },
        setFirstName(newFirstName) {
            Object.defineProperty(this, 'firstName', {
                writable: true,
              })
              this.firstName = newFirstName;
              Object.defineProperty(this, 'firstName', {
                writable: false,
              })
        },
        setLastName(newLastName) {
            Object.defineProperty(this, 'lastName', {
                writable: true,
              })
              this.LastName = newLastName;
              Object.defineProperty(this, 'lastName', {
                writable: false,
              })
        },
    }
}

let newUser = createNewUser(); 

Object.defineProperties(newUser, {
    firstName: {
      writable: false,
    },
    lastName: {
      writable: false,
    }
  })
  
  
console.log(newUser.getPassword())
console.log(newUser.getAge())
console.log(newUser.getLogin())
console.log(newUser)




